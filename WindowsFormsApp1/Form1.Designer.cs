﻿namespace WindowsFormsApp1
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
			System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
			System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
			this.MyChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.JobBtn = new System.Windows.Forms.Button();
			this.Atbx = new System.Windows.Forms.TextBox();
			this.Btbx = new System.Windows.Forms.TextBox();
			this.SQtbx = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.Htbx = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.S20 = new System.Windows.Forms.RadioButton();
			this.S21 = new System.Windows.Forms.RadioButton();
			this.S22 = new System.Windows.Forms.RadioButton();
			this.S30 = new System.Windows.Forms.RadioButton();
			this.S31 = new System.Windows.Forms.RadioButton();
			this.S32 = new System.Windows.Forms.RadioButton();
			this.S40 = new System.Windows.Forms.RadioButton();
			this.S41 = new System.Windows.Forms.RadioButton();
			this.S42 = new System.Windows.Forms.RadioButton();
			this.S50 = new System.Windows.Forms.RadioButton();
			((System.ComponentModel.ISupportInitialize)(this.MyChart)).BeginInit();
			this.tableLayoutPanel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// MyChart
			// 
			chartArea2.Name = "ChartArea1";
			this.MyChart.ChartAreas.Add(chartArea2);
			this.MyChart.Dock = System.Windows.Forms.DockStyle.Fill;
			this.MyChart.Location = new System.Drawing.Point(2, 2);
			this.MyChart.Margin = new System.Windows.Forms.Padding(2);
			this.MyChart.Name = "MyChart";
			series3.ChartArea = "ChartArea1";
			series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
			series3.Name = "PersonalSeries";
			series4.ChartArea = "ChartArea1";
			series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
			series4.Name = "Series2";
			this.MyChart.Series.Add(series3);
			this.MyChart.Series.Add(series4);
			this.MyChart.Size = new System.Drawing.Size(641, 443);
			this.MyChart.TabIndex = 0;
			this.MyChart.Text = "chart1";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.84769F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.15231F));
			this.tableLayoutPanel1.Controls.Add(this.MyChart, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.JobBtn, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(862, 447);
			this.tableLayoutPanel1.TabIndex = 1;
			// 
			// JobBtn
			// 
			this.JobBtn.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.JobBtn.Location = new System.Drawing.Point(647, 2);
			this.JobBtn.Margin = new System.Windows.Forms.Padding(2);
			this.JobBtn.Name = "JobBtn";
			this.JobBtn.Size = new System.Drawing.Size(213, 63);
			this.JobBtn.TabIndex = 1;
			this.JobBtn.Text = "Запуск";
			this.JobBtn.UseVisualStyleBackColor = true;
			this.JobBtn.Click += new System.EventHandler(this.JobBtn_Click);
			// 
			// Atbx
			// 
			this.Atbx.Location = new System.Drawing.Point(729, 72);
			this.Atbx.Margin = new System.Windows.Forms.Padding(2);
			this.Atbx.Multiline = true;
			this.Atbx.Name = "Atbx";
			this.Atbx.Size = new System.Drawing.Size(55, 26);
			this.Atbx.TabIndex = 2;
			this.Atbx.Text = "1";
			// 
			// Btbx
			// 
			this.Btbx.Location = new System.Drawing.Point(729, 102);
			this.Btbx.Margin = new System.Windows.Forms.Padding(2);
			this.Btbx.Multiline = true;
			this.Btbx.Name = "Btbx";
			this.Btbx.Size = new System.Drawing.Size(55, 27);
			this.Btbx.TabIndex = 3;
			this.Btbx.Text = "10";
			// 
			// SQtbx
			// 
			this.SQtbx.Location = new System.Drawing.Point(729, 165);
			this.SQtbx.Margin = new System.Windows.Forms.Padding(2);
			this.SQtbx.Multiline = true;
			this.SQtbx.Name = "SQtbx";
			this.SQtbx.Size = new System.Drawing.Size(55, 30);
			this.SQtbx.TabIndex = 5;
			this.SQtbx.Text = "100";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(674, 78);
			this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(13, 13);
			this.label1.TabIndex = 6;
			this.label1.Text = "a";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(676, 108);
			this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(13, 13);
			this.label2.TabIndex = 7;
			this.label2.Tag = "";
			this.label2.Text = "b";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(678, 173);
			this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(11, 13);
			this.label4.TabIndex = 9;
			this.label4.Text = "*";
			// 
			// Htbx
			// 
			this.Htbx.Location = new System.Drawing.Point(729, 132);
			this.Htbx.Margin = new System.Windows.Forms.Padding(2);
			this.Htbx.Multiline = true;
			this.Htbx.Name = "Htbx";
			this.Htbx.Size = new System.Drawing.Size(55, 28);
			this.Htbx.TabIndex = 4;
			this.Htbx.Text = "1";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(675, 141);
			this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(13, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "h";
			// 
			// S20
			// 
			this.S20.AutoSize = true;
			this.S20.Location = new System.Drawing.Point(680, 205);
			this.S20.Name = "S20";
			this.S20.Size = new System.Drawing.Size(44, 17);
			this.S20.TabIndex = 10;
			this.S20.TabStop = true;
			this.S20.Text = "S20";
			this.S20.UseVisualStyleBackColor = true;
			// 
			// S21
			// 
			this.S21.AutoSize = true;
			this.S21.Location = new System.Drawing.Point(680, 223);
			this.S21.Name = "S21";
			this.S21.Size = new System.Drawing.Size(44, 17);
			this.S21.TabIndex = 11;
			this.S21.TabStop = true;
			this.S21.Text = "S21";
			this.S21.UseVisualStyleBackColor = true;
			// 
			// S22
			// 
			this.S22.AutoSize = true;
			this.S22.Location = new System.Drawing.Point(680, 244);
			this.S22.Name = "S22";
			this.S22.Size = new System.Drawing.Size(44, 17);
			this.S22.TabIndex = 12;
			this.S22.TabStop = true;
			this.S22.Text = "S22";
			this.S22.UseVisualStyleBackColor = true;
			// 
			// S30
			// 
			this.S30.AutoSize = true;
			this.S30.Location = new System.Drawing.Point(680, 264);
			this.S30.Name = "S30";
			this.S30.Size = new System.Drawing.Size(44, 17);
			this.S30.TabIndex = 13;
			this.S30.TabStop = true;
			this.S30.Text = "S30";
			this.S30.UseVisualStyleBackColor = true;
			// 
			// S31
			// 
			this.S31.AutoSize = true;
			this.S31.Location = new System.Drawing.Point(680, 285);
			this.S31.Name = "S31";
			this.S31.Size = new System.Drawing.Size(44, 17);
			this.S31.TabIndex = 14;
			this.S31.TabStop = true;
			this.S31.Text = "S31";
			this.S31.UseVisualStyleBackColor = true;
			// 
			// S32
			// 
			this.S32.AutoSize = true;
			this.S32.Location = new System.Drawing.Point(681, 303);
			this.S32.Name = "S32";
			this.S32.Size = new System.Drawing.Size(44, 17);
			this.S32.TabIndex = 15;
			this.S32.TabStop = true;
			this.S32.Text = "S32";
			this.S32.UseVisualStyleBackColor = true;
			// 
			// S40
			// 
			this.S40.AutoSize = true;
			this.S40.Location = new System.Drawing.Point(681, 323);
			this.S40.Name = "S40";
			this.S40.Size = new System.Drawing.Size(44, 17);
			this.S40.TabIndex = 16;
			this.S40.TabStop = true;
			this.S40.Text = "S40";
			this.S40.UseVisualStyleBackColor = true;
			// 
			// S41
			// 
			this.S41.AutoSize = true;
			this.S41.Location = new System.Drawing.Point(681, 343);
			this.S41.Name = "S41";
			this.S41.Size = new System.Drawing.Size(44, 17);
			this.S41.TabIndex = 17;
			this.S41.TabStop = true;
			this.S41.Text = "S41";
			this.S41.UseVisualStyleBackColor = true;
			// 
			// S42
			// 
			this.S42.AutoSize = true;
			this.S42.Location = new System.Drawing.Point(681, 361);
			this.S42.Name = "S42";
			this.S42.Size = new System.Drawing.Size(44, 17);
			this.S42.TabIndex = 18;
			this.S42.TabStop = true;
			this.S42.Text = "S42";
			this.S42.UseVisualStyleBackColor = true;
			// 
			// S50
			// 
			this.S50.AutoSize = true;
			this.S50.Location = new System.Drawing.Point(681, 381);
			this.S50.Name = "S50";
			this.S50.Size = new System.Drawing.Size(44, 17);
			this.S50.TabIndex = 19;
			this.S50.TabStop = true;
			this.S50.Text = "S50";
			this.S50.UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(862, 447);
			this.Controls.Add(this.S50);
			this.Controls.Add(this.S42);
			this.Controls.Add(this.S41);
			this.Controls.Add(this.S40);
			this.Controls.Add(this.S32);
			this.Controls.Add(this.S31);
			this.Controls.Add(this.S30);
			this.Controls.Add(this.S22);
			this.Controls.Add(this.S21);
			this.Controls.Add(this.S20);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.SQtbx);
			this.Controls.Add(this.Htbx);
			this.Controls.Add(this.Btbx);
			this.Controls.Add(this.Atbx);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Margin = new System.Windows.Forms.Padding(2);
			this.Name = "Form1";
			this.Text = "Form1";
			((System.ComponentModel.ISupportInitialize)(this.MyChart)).EndInit();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DataVisualization.Charting.Chart MyChart;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.Button JobBtn;
		private System.Windows.Forms.TextBox Atbx;
		private System.Windows.Forms.TextBox Btbx;
		private System.Windows.Forms.TextBox SQtbx;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox Htbx;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.RadioButton S20;
		private System.Windows.Forms.RadioButton S21;
		private System.Windows.Forms.RadioButton S22;
		private System.Windows.Forms.RadioButton S30;
		private System.Windows.Forms.RadioButton S31;
		private System.Windows.Forms.RadioButton S32;
		private System.Windows.Forms.RadioButton S40;
		private System.Windows.Forms.RadioButton S41;
		private System.Windows.Forms.RadioButton S42;
		private System.Windows.Forms.RadioButton S50;
	}
}

