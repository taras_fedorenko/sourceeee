﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WindowsFormsApp1
{
	public static class M
	{

		private static int steps;
		private static List<double> X;

		static M()
		{
			steps = Form1.sq;
			X = Enumerable.Range(0, steps)
				 .Select(i => Math.Round(-1 + 2 * ((double)i / (steps - 1)), 2)).ToList();
		}

		public static List<double> Equation(List<double> Pi, Func<double, double, double, double, double, double> S)//s21
		{
			var res = new List<double>();

			for (int i = 1; i < Pi.Count - 2; i++)
			{
				for (int r = 0; r < X.Count; r++)
				{
					res.Add(S(Pi[i - 1], Pi[i], Pi[i + 1], Pi[i + 2], X[r]));
				}
			}
			return res;
		}

		public static List<double> Equation(List<double> Pi, Func<double, double, double, double, double> S)//s20
		{
			var res = new List<double>();

			for (int i = 1; i < Pi.Count - 1; i++)
			{
				for (int r = 0; r < X.Count; r++)
				{
					res.Add(S(Pi[i - 1], Pi[i], Pi[i + 1], X[r]));
				}
			}
			return res;
		}

		public static List<double> Equation(List<double> Pi, Func<double, double, double, double, double, double, double, double, double> S)//s22
		{
			var res = new List<double>();

			for (int i = 3; i < Pi.Count - 4; i++)
			{
				for (int r = 0; r < X.Count; r++)
				{
					res.Add(S(Pi[i - 3], Pi[i - 2], Pi[i - 1], Pi[i], Pi[i + 1], Pi[i + 2], Pi[i + 3], X[r]));
				}
			}
			return res;
		}

		public static List<double> Equation(List<double> Pi, Func<double, double, double, double, double, double> S, bool s30)//s30
		{
			var res = new List<double>();

			for (int i = 1; i < Pi.Count - 2; i++)
			{
				for (int r = 0; r < X.Count; r++)
				{
					res.Add(S(Pi[i - 1], Pi[i], Pi[i + 1], Pi[i + 2], X[r]));
				}
			}
			return res;
		}

		public static List<double> Equation(List<double> Pi, Func<double, double, double, double, double, double, double, double> S)//s31
		{
			var res = new List<double>();

			for (int i = 2; i < Pi.Count - 3; i++)
			{
				for (int r = 0; r < X.Count; r++)
				{
					res.Add(S(Pi[i - 2], Pi[i - 1], Pi[i], Pi[i + 1], Pi[i + 2], Pi[i + 3], X[r]));
				}
			}
			return res;
		}

		public static List<double> Equation(List<double> Pi, Func<double, double, double, double, double, double, double, double, double, double> S)//s32
		{
			var res = new List<double>();

			for (int i = 3; i < Pi.Count - 4; i++)
			{
				for (int r = 0; r < X.Count; r++)
				{
					res.Add(S(Pi[i - 3], Pi[i - 2], Pi[i - 1], Pi[i], Pi[i + 1], Pi[i + 2], Pi[i + 3], Pi[i + 4], X[r]));
				}
			}
			return res;
		}

		public static List<double> Equation(List<double> Pi, Func<double, double, double, double, double, double, double> S)//s40
		{
			var res = new List<double>();

			for (int i = 2; i < Pi.Count - 2; i++)
			{
				for (int r = 0; r < X.Count; r++)
				{
					res.Add(S(Pi[i - 2], Pi[i - 1], Pi[i], Pi[i + 1], Pi[i + 2], X[r]));
				}
			}
			return res;
		}

		public static List<double> Equation(List<double> Pi, Func<double, double, double, double, double, double, double, double, double> S, bool s41)//s41
		{
			var res = new List<double>();

			for (int i = 3; i < Pi.Count - 3; i++)
			{
				for (int r = 0; r < X.Count; r++)
				{
					res.Add(S(Pi[i - 3], Pi[i - 2], Pi[i - 1], Pi[i], Pi[i + 1], Pi[i + 2], Pi[i + 3], X[r]));
				}
			}
			return res;
		}

		public static List<double> Equation(List<double> Pi, Func<double, double, double, double, double, double, double, double, double, double, double> S)//s42
		{
			var res = new List<double>();

			for (int i = 4; i < Pi.Count - 4; i++)
			{
				for (int r = 0; r < X.Count; r++)
				{
					res.Add(S(Pi[i - 4], Pi[i - 3], Pi[i - 2], Pi[i - 1], Pi[i], Pi[i + 1], Pi[i + 2], Pi[i + 3], Pi[i + 4], X[r]));
				}
			}
			return res;
		}

		public static List<double> Equation(List<double> Pi, Func<double, double, double, double, double, double, double, double> S, bool s50)//s50
		{
			var res = new List<double>();

			for (int i = 2; i < Pi.Count - 3; i++)
			{
				for (int r = 0; r < X.Count; r++)
				{
					res.Add(S(Pi[i - 2], Pi[i - 1], Pi[i], Pi[i + 1], Pi[i + 2], Pi[i + 3], X[r]));
				}
			}
			return res;
		}

	}
}
